var search = new Bloodhound({
    name: 'search',
    datumTokenizer: function(d){
        return Bloodhound.tokenizers.whitespace(d.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: 'http://lookup.hotels.com/2/suggest/v1/json?locale=en_US&boostConfig=config-boost-2&excludeLpa=true&query=%QUERY',
        wildcard: '%QUERY',
        filter: function (result) {
            return $.map(result.suggestions, function(suggestion) {
                return $.map(suggestion.entities, function(item) {
                    return {
                        value: item.caption,
                        name: item.name
                    }
                });
            });
        }
    }
});

search.initialize();

$('#bloodhound .typeahead').typeahead(null,{
    displayKey: 'name',
    source: search.ttAdapter(),
    templates: {
        suggestion: function (result) {
            return '<div>' + result.value + '</div>';
        }
    }
});

$('.datepicker-from, .datepicker-to').datepicker();

$(document).ready(function(){
    $('body').on('click', '.btn-primary', function(){
        chrome.tabs.create({url: 'http://www.hotels.com/'});
        return false;
    });
});


var SearchQueryModule = React.createClass({displayName: "SearchQueryModule",
    handleChange: function(e) {
        console.log(e.target.value);
    },

    componentDidMount: function() {
        React.findDOMNode(this.refs.myTextInput).focus();
        $(React.findDOMNode(this.refs.myTextInput)).typeahead(null, {
            displayKey: 'name',
            source: search.ttAdapter(),
            templates: {
                suggestion: function (result) {
                    return '<div>' + result.value + '</div>';
                }
            }
        });
    },

    render: function() {
        console.log('resd');
        return (
            React.createElement("div", {id: "bloodhound"}, React.createElement("div", null,
                React.createElement("input", {className: "typeahead form-control", type: "text", placeholder: "Destination, hotel, airport or landmark", ref: "myTextInput", name: "sahan", onChange: this.handleChange, onBlur: this.handleChange}))
            )
        );
    }
});

React.render(
  React.createElement(SearchQueryModule, {query: ""}), document.getElementById('example')
);

