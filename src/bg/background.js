chrome.browserAction.setBadgeBackgroundColor({ color: [255, 0, 0, 255] });
function getCookies(domain, name, callback) {
    chrome.cookies.get({"url": domain, "name": name}, function(cookie) {
        if(callback) {
            callback(cookie.value);
        }
    });
}
getCookies("http://www.hotels.com", "SESSID", function(id) {
    chrome.browserAction.setBadgeText({text: id});
});

//example of using a message handler from the inject scripts
chrome.extension.onMessage.addListener(
    function(request, sender, sendResponse) {
        chrome.pageAction.show(sender.tab.id);
        sendResponse();
    }
);

